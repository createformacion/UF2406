﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace webserver
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri direccion = new Uri("http://localhost:5555");

            ServiceMetadataBehavior b = new ServiceMetadataBehavior();
            b.HttpGetEnabled = true;

            ServiceHost host = new ServiceHost(typeof(StarService.StarService), direccion);
            //WebHttpBehavior wb = new WebHttpBehavior();
            //WebServiceHost host = new WebServiceHost(typeof(EnvioJson.PersonaJson), direccion);

            ServiceEndpoint ep = host.AddServiceEndpoint(typeof(StarService.IStarService), new WSHttpBinding(), "Servicio");
            //ep.EndpointBehaviors.Add(wb);
            host.Description.Behaviors.Add(b);

            //foreach (ServiceEndpoint ep in host.Description.Endpoints)
            //{
            //    WebInvokeAttribute webInvokeAttribute = new WebInvokeAttribute();
            //    webInvokeAttribute.RequestFormat = WebMessageFormat.Json;
            //    webInvokeAttribute.ResponseFormat = WebMessageFormat.Json;
            //    webInvokeAttribute.Method = "HTTP";
            //    webInvokeAttribute.BodyStyle = WebMessageBodyStyle.Bare;

            //    foreach (var operation in ep.Contract.Operations)
            //    {
            //        operation.OperationBehaviors.Add(webInvokeAttribute);
            //    }
            //    //ep.Contract.Operations.Find("<<opeartionname>>").Behaviors.Add(webInvokeAttribute);
            //}


            host.Open();
            Console.WriteLine("Servidor arrancado con el servicio WCF escuchando");
            Console.ReadLine();
            host.Close();

        }
    }
}
