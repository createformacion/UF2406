﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace StarService
{
    [ServiceContract]
    public interface IStarService
    {
        [OperationContract]
        List<string> GetTodasLasBirras();

        [OperationContract]
        int GetPrecioDe(string marca);

    }

    [DataContract]
    public class Cerveza
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Graduacion { get; set; }

        [DataMember]
        public int Precio { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public bool SinAlcohol { get; set; }
    }
}
