﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace StarService
{
    public class StarService : IStarService
    {
        private BirraContext db = new BirraContext();
        public int GetPrecioDe(string marca)
        {
            Cerveza birra; 
            using (BirraContext b = new BirraContext())
            {
                birra = b.Cervezas.Where(x => x.Marca == marca).FirstOrDefault();
            }
                return birra.Precio;
        }

        public List<string> GetTodasLasBirras()
        {
            return db.Cervezas.Select(x => x.Marca).ToList();
        }
    }
}
